# Crypto Manager

Este es un proyecto de ejemplo para un administrador de criptomonedas utilizando Spring Boot.

## Requisitos previos

- Java 17 o superior
- Maven 3.6.0 o superior
- Git

## Clonar el repositorio

```bash
git clone https://gitlab.com/facundocardenas974/crypto-manager.git

cd crypto-manager

## Para levantar el proyecto una vez clonado

mvn clean install

mvn spring-boot:run


## Para acceder a Swagger localmente y ver la documentacion de la API:

http://localhost:8080/swagger-ui/index.html