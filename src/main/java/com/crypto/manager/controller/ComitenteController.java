package com.crypto.manager.controller;

import com.crypto.manager.dtos.ComitenteDTO;
import com.crypto.manager.services.ComitenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/comitentes")
public class ComitenteController {

    @Autowired
    private ComitenteService comitenteService;

    @GetMapping
    public List<ComitenteDTO> getAllComitentes() {
        return comitenteService.getAllComitentes();
    }

    @PostMapping
    public ComitenteDTO createComitente(@RequestBody ComitenteDTO comitenteDTO) {
        return comitenteService.createComitente(comitenteDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ComitenteDTO> updateComitente(@PathVariable Long id, @RequestBody ComitenteDTO comitenteDTO) {
        ComitenteDTO updatedComitente = comitenteService.updateComitente(id, comitenteDTO);
        return ResponseEntity.ok(updatedComitente);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteComitente(@PathVariable Long id) {
        comitenteService.deleteComitente(id);
        return ResponseEntity.noContent().build();
    }
}
