package com.crypto.manager.controller;

import com.crypto.manager.dtos.MercadoDTO;
import com.crypto.manager.services.MercadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/mercados")
public class MercadoController {

    @Autowired
    private MercadoService mercadoService;

    @GetMapping
    public List<MercadoDTO> getAllMercados() {
        return mercadoService.getAllMercados();
    }

    @PostMapping
    public MercadoDTO createMercado(@RequestBody MercadoDTO mercadoDTO) {
        return mercadoService.createMercado(mercadoDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<MercadoDTO> updateMercado(@PathVariable Long id, @RequestBody MercadoDTO mercadoDTO) {
        MercadoDTO updatedMercado = mercadoService.updateMercado(id, mercadoDTO);
        return ResponseEntity.ok(updatedMercado);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteMercado(@PathVariable Long id) {
        mercadoService.deleteMercado(id);
        return ResponseEntity.noContent().build();
    }
}
