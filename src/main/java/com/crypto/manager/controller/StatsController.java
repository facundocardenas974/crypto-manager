package com.crypto.manager.controller;


import com.crypto.manager.services.StatsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/stats")
@Tag(name = "Stats", description = "API para operaciones estadísticas")
public class StatsController {

    private final StatsService statsService;

    @Autowired
    public StatsController(StatsService statsService) {
        this.statsService = statsService;
    }

    @Operation(summary = "Calcular estadísticas", description = "Calcula las estadísticas para todos los comitentes")
    @PostMapping("/calculate")
    public void calculateStats() {
        statsService.calculateStats();
    }
}