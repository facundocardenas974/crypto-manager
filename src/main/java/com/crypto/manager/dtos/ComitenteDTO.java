package com.crypto.manager.dtos;

import java.util.Set;

public class ComitenteDTO {
    private Long id;
    private String descripcion;
    private Set<Long> mercadoIds;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Set<Long> getMercadoIds() {
        return mercadoIds;
    }

    public void setMercadoIds(Set<Long> mercadoIds) {
        this.mercadoIds = mercadoIds;
    }
}
