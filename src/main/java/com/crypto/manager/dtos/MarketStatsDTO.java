package com.crypto.manager.dtos;

import java.math.BigDecimal;

public class MarketStatsDTO {
    private String market;
    private BigDecimal percentage;

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }
}

