package com.crypto.manager.dtos;


import java.math.BigDecimal;
import java.util.List;

public class StatsDTO {
    private String country;
    private List<MarketStatsDTO> marketStats;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<MarketStatsDTO> getMarketStats() {
        return marketStats;
    }

    public void setMarketStats(List<MarketStatsDTO> marketStats) {
        this.marketStats = marketStats;
    }
}
