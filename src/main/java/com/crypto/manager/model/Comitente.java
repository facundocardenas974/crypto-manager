package com.crypto.manager.model;

import jakarta.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Comitente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String descripcion;

    @ManyToMany
    @JoinTable(
            name = "comitente_mercado",
            joinColumns = @JoinColumn(name = "comitente_id"),
            inverseJoinColumns = @JoinColumn(name = "mercado_id")
    )
    private Set<Mercado> mercados = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Set<Mercado> getMercados() {
        return mercados;
    }

    public void setMercados(Set<Mercado> mercados) {
        this.mercados = mercados;
    }
}

