package com.crypto.manager.repositories;

import com.crypto.manager.model.Comitente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComitenteRepository extends JpaRepository<Comitente, Long> {
    boolean existsByDescripcion(String descripcion);
}

