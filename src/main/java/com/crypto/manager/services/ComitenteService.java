package com.crypto.manager.services;

import com.crypto.manager.dtos.ComitenteDTO;
import com.crypto.manager.model.Comitente;
import com.crypto.manager.model.Mercado;
import com.crypto.manager.repositories.ComitenteRepository;
import com.crypto.manager.repositories.MercadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ComitenteService {

    @Autowired
    private ComitenteRepository comitenteRepository;

    @Autowired
    private MercadoRepository mercadoRepository;

    public List<ComitenteDTO> getAllComitentes() {
        return comitenteRepository.findAll().stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    public ComitenteDTO createComitente(ComitenteDTO comitenteDTO) {
        if (comitenteRepository.existsByDescripcion(comitenteDTO.getDescripcion())) {
            throw new RuntimeException("Comitente ya existe");
        }

        Comitente comitente = new Comitente();
        comitente.setDescripcion(comitenteDTO.getDescripcion());

        Set<Mercado> mercados = new HashSet<>(mercadoRepository.findAllById(comitenteDTO.getMercadoIds()));
        comitente.setMercados(mercados);

        Comitente savedComitente = comitenteRepository.save(comitente);
        return convertToDto(savedComitente);
    }

    public ComitenteDTO updateComitente(Long id, ComitenteDTO comitenteDTO) {
        Comitente comitente = comitenteRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Comitente no encontrado"));

        comitente.setDescripcion(comitenteDTO.getDescripcion());

        Set<Mercado> mercados = new HashSet<>(mercadoRepository.findAllById(comitenteDTO.getMercadoIds()));
        comitente.setMercados(mercados);

        Comitente updatedComitente = comitenteRepository.save(comitente);
        return convertToDto(updatedComitente);
    }

    public void deleteComitente(Long id) {
        Comitente comitente = comitenteRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Comitente no encontrado"));

        comitenteRepository.delete(comitente);
    }

    private ComitenteDTO convertToDto(Comitente comitente) {
        ComitenteDTO comitenteDTO = new ComitenteDTO();
        comitenteDTO.setId(comitente.getId());
        comitenteDTO.setDescripcion(comitente.getDescripcion());
        comitenteDTO.setMercadoIds(comitente.getMercados().stream()
                .map(Mercado::getId)
                .collect(Collectors.toSet()));
        return comitenteDTO;
    }
}
