package  com.crypto.manager.services;

import com.crypto.manager.dtos.MercadoDTO;
import com.crypto.manager.model.Mercado;
import com.crypto.manager.model.Pais;
import com.crypto.manager.repositories.MercadoRepository;
import com.crypto.manager.repositories.PaisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MercadoService {

    @Autowired
    private MercadoRepository mercadoRepository;

    @Autowired
    private PaisRepository paisRepository;

    public List<MercadoDTO> getAllMercados() {
        return mercadoRepository.findAll().stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    public MercadoDTO createMercado(MercadoDTO mercadoDTO) {
        Pais pais = paisRepository.findById(mercadoDTO.getPaisId())
                .orElseThrow(() -> new RuntimeException("País no válido"));

        Mercado mercado = new Mercado();
        mercado.setCodigo(mercadoDTO.getCodigo());
        mercado.setDescripcion(mercadoDTO.getDescripcion());
        mercado.setPais(pais);

        Mercado savedMercado = mercadoRepository.save(mercado);
        return convertToDto(savedMercado);
    }

    public MercadoDTO updateMercado(Long id, MercadoDTO mercadoDTO) {
        Mercado mercado = mercadoRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Mercado no encontrado"));

        Pais pais = paisRepository.findById(mercadoDTO.getPaisId())
                .orElseThrow(() -> new RuntimeException("País no válido"));

        mercado.setCodigo(mercadoDTO.getCodigo());
        mercado.setDescripcion(mercadoDTO.getDescripcion());
        mercado.setPais(pais);

        Mercado updatedMercado = mercadoRepository.save(mercado);
        return convertToDto(updatedMercado);
    }

    public void deleteMercado(Long id) {
        Mercado mercado = mercadoRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Mercado no encontrado"));

        mercadoRepository.delete(mercado);
    }

    private MercadoDTO convertToDto(Mercado mercado) {
        MercadoDTO mercadoDTO = new MercadoDTO();
        mercadoDTO.setId(mercado.getId());
        mercadoDTO.setCodigo(mercado.getCodigo());
        mercadoDTO.setDescripcion(mercado.getDescripcion());
        mercadoDTO.setPaisId(mercado.getPais().getId());
        return mercadoDTO;
    }
}
