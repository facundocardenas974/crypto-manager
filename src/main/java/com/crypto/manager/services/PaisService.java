package com.crypto.manager.services;


import com.crypto.manager.repositories.PaisRepository;
import com.crypto.manager.model.Pais;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaisService {

    @Autowired
    private PaisRepository paisRepository;

    public List<Pais> getAllPaises() {
        return paisRepository.findAll();
    }

    public Pais createPais(Pais pais) {
        return paisRepository.save(pais);
    }

    public Pais updatePais(Long id, Pais paisDetails) {
        Pais pais = paisRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("País no encontrado"));

        pais.setNombre(paisDetails.getNombre());
        return paisRepository.save(pais);
    }

    public void deletePais(Long id) {
        Pais pais = paisRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("País no encontrado"));

        paisRepository.delete(pais);
    }
}
