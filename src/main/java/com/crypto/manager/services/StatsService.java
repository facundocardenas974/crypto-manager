package com.crypto.manager.services;

import com.crypto.manager.dtos.MarketStatsDTO;
import com.crypto.manager.dtos.StatsDTO;
import com.crypto.manager.model.Comitente;
import com.crypto.manager.model.Mercado;
import com.crypto.manager.repositories.ComitenteRepository;
import com.crypto.manager.repositories.MercadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@Service
public class StatsService {

    @Autowired
    private ComitenteRepository comitenteRepository;

    @Autowired
    private MercadoRepository mercadoRepository;

    public List<StatsDTO> calculateStats() {
        List<StatsDTO> statsList = new ArrayList<>();

        List<Comitente> comitentes = comitenteRepository.findAll();

        // Mapa para almacenar el recuento de comitentes por país y mercado
        Map<String, Map<String, Long>> comitentesPorPaisYMercado = new HashMap<>();

        // Conteo total de comitentes por país
        Map<String, Long> totalComitentesPorPais = new HashMap<>();

        for (Comitente comitente : comitentes) {
            // Obtener el conjunto de mercados del comitente
            Set<Mercado> mercados = comitente.getMercados();

            // Verificar si el comitente tiene mercados asociados
            if (mercados != null && !mercados.isEmpty()) {
                for (Mercado mercado : mercados) {
                    String pais = mercado.getPais().getNombre();
                    String mercadoNombre = mercado.getDescripcion();

                    // Actualizar el recuento total de comitentes por país
                    totalComitentesPorPais.put(pais, totalComitentesPorPais.getOrDefault(pais, 0L) + 1);

                    // Actualizar el recuento de comitentes por país y mercado
                    Map<String, Long> comitentesPorMercado = comitentesPorPaisYMercado.getOrDefault(pais, new HashMap<>());
                    comitentesPorMercado.put(mercadoNombre, comitentesPorMercado.getOrDefault(mercadoNombre, 0L) + 1);
                    comitentesPorPaisYMercado.put(pais, comitentesPorMercado);
                }
            }
        }

        for (Map.Entry<String, Map<String, Long>> entry : comitentesPorPaisYMercado.entrySet()) {
            String pais = entry.getKey();
            Map<String, Long> comitentesPorMercado = entry.getValue();
            long totalComitentesEnPais = totalComitentesPorPais.get(pais);

            StatsDTO statsDTO = new StatsDTO();
            statsDTO.setCountry(pais);
            List<MarketStatsDTO> marketStatsList = new ArrayList<>();

            for (Map.Entry<String, Long> mercadoEntry : comitentesPorMercado.entrySet()) {
                String mercadoNombre = mercadoEntry.getKey();
                Long comitentesEnMercado = mercadoEntry.getValue();
                BigDecimal percentage = BigDecimal.valueOf(comitentesEnMercado)
                        .divide(BigDecimal.valueOf(totalComitentesEnPais), 2, RoundingMode.HALF_UP)
                        .multiply(BigDecimal.valueOf(100));

                MarketStatsDTO marketStatsDTO = new MarketStatsDTO();
                marketStatsDTO.setMarket(mercadoNombre);
                marketStatsDTO.setPercentage(percentage);
                marketStatsList.add(marketStatsDTO);
            }

            statsDTO.setMarketStats(marketStatsList);
            statsList.add(statsDTO);
        }

        return statsList;
    }

}